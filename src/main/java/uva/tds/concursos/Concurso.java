package uva.tds.concursos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class Concurso<E> implements IConcurso<E>{
	protected int maximosNominados;
	protected boolean cerrado;
	protected Map<E,Integer> concurso; // pares (nominado, votos)
	protected Calendar fIniVotaciones;
	protected Calendar fFinVotaciones;

	public Concurso(int maximosNominados, Calendar fIniVotaciones, Calendar fFinVotaciones) {
		if (maximosNominados<=0){
			throw new IllegalArgumentException("No se aceptan números menores o iguales que cero.");
		}
		this.maximosNominados = maximosNominados;
		this.fIniVotaciones = fIniVotaciones;
		this.fFinVotaciones = fFinVotaciones;
		concurso = new LinkedHashMap<E,Integer>();
		cerrado = false;
	}

	public boolean isCerrado() {
		return cerrado;
	}

	public void cerrar() {
		if (cerrado){
			throw new IllegalStateException("No se puede volver a cerrar un concurso ya cerrado.");
		}
		if (fFinVotaciones.after(Calendar.getInstance())){
			throw new IllegalStateException("No se puede cerrar en periodo de votaciones.");
		}
		cerrado = true;
	}

	public void nominar(E p) {
		if (p==null){
			throw new IllegalArgumentException("No se puede nominar un null.");
		}
		if (concurso.containsKey(p)){
			throw new IllegalArgumentException("Producto ya nominado.");
		}
		if (concurso.size()==maximosNominados){
			throw new IllegalStateException("El concurso ya esta lleno.");
		}
		if (cerrado){
			throw new IllegalStateException("El concurso ya se ha cerrado.");
		}
		concurso.put(p, 0);
	}

	public void nominar(E[] productos) {
		if (productos==null){
			throw new IllegalArgumentException("No se puede nominar un null.");
		}
		for(int i=0; i< productos.length; i++){
			E p = productos[i];
			if (p==null){
				throw new IllegalArgumentException("No se puede nominar un null.");
			}
			if (concurso.containsKey(p)){
				throw new IllegalArgumentException("Producto ya nominado.");
			}
			for (int j=i+1; j<productos.length; j++){
				if (p.equals(productos[j])){
					throw new IllegalArgumentException("No se puede nominar varias veces el mismo producto");
				}
			}
		}
		if (concurso.size() + productos.length > maximosNominados){
			throw new IllegalStateException("El concurso no acepta algunos de los productos por estar casi lleno.");
		}
		if (cerrado){
			throw new IllegalStateException("El concurso ya se ha cerrado.");
		}
		for(E p : productos){
			/* TODO: podemos evitar repetición de código si empleamos el metodo nominar para un solo producto */
			concurso.put(p, 0); 
		}
	}

	public void votar(E p) {
		if (cerrado){
			throw new IllegalStateException("El concurso ya se ha cerrado.");
		}
		if (fIniVotaciones.after(Calendar.getInstance())){
			throw new IllegalStateException("El periodo de votaciones aun no ha comenzado.");
		}
		if (fFinVotaciones.before(Calendar.getInstance())){
			throw new IllegalStateException("El periodo de votaciones ha terminado.");
		}
		if (p==null){
			throw new IllegalArgumentException("No se puede votar un null.");
		}
		if (!concurso.containsKey(p)){
			throw new IllegalArgumentException("El producto no ha sido nominado.");
		}
		int votos = concurso.get(p);
		concurso.put(p, votos+1);
	}

	public IRanking<E> obtenerRanking() {
		return obtenerRanking(10);
	}

	public IRanking<E> obtenerRanking(int top) {
		if (!cerrado){
			throw new IllegalStateException("El concurso ya se ha cerrado.");
		}
		if (top > concurso.size()){
			throw new IllegalStateException("El concurso tiene menos nominados que los solicitados.");
		}
		if (top<=0){
			throw new IllegalArgumentException("No se aceptan numeros menores o iguales que cero.");
		}
		
		IRanking<E> ranking = nuevoRanking(top);
	
		List<Map.Entry<E,Integer>> productos = new LinkedList<>(concurso.entrySet());
		Collections.sort(productos, new Comparator<Map.Entry<E,Integer>>(){
			public int compare(Map.Entry<E, Integer> v1,
							   Map.Entry<E, Integer> v2) {
				// El objetivo son los productos con más votos: se invierte el orden multiplicando por (-1)
				return (-1)*(v1.getValue()).compareTo(v2.getValue());
			}
		});
		
		for(int i=0 ; i<top ; i++){
			Map.Entry<E, Integer> productoOrd = productos.get(i);
			ranking.addProduct(productoOrd.getKey());
		}
		return ranking;
	}
	
	public <E> Ranking<E> nuevoRanking(int n){
		return new Ranking<E>(n);
	}
}
