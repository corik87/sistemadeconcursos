package uva.tds.concursos;

import java.util.ArrayList;
import java.util.Map;

public interface IRanking<P> {
	
	public int getSize();

	public int getMaxSize();

	public void addProduct(P producto);

	public P getProductByPosition(int posicion);

	public P getProduct(String prodReference);

	public int getProductPosition(P product);
	
	public int getProductPosition(String prodReference);
	
	public ArrayList<P> getProducts();

	public boolean containsProduct(P producto);

	public boolean containsProduct(String prodReference);

	public Map<P, Integer> compareToRanking(IRanking<P> r);

}