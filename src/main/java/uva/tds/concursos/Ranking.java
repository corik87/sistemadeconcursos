package uva.tds.concursos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class Ranking<P> implements IRanking<P> {
	
	private ArrayList<P> productos;
	private int maxSize;
	private int size;
	
	public Ranking(){
		this(10);
	}
	
	public Ranking(int size){
		this.size = 0;
		maxSize = size;
		productos = new ArrayList<P>(maxSize);
		for(int i=0; i<maxSize; i++){
			productos.add(null);
		}
	}
	
	@Override
	public int getSize(){
		return size;
	}
	
	@Override
	public int getMaxSize() {
		return maxSize;
	}
	
	@Override
	public void addProduct(P producto) {
		if(size < maxSize){
			if(!containsProduct(producto)){
				productos.set(size,producto);
				size++;
			}else throw new IllegalArgumentException("El producto pasado como"
					+ " argumento ya aparece en el ranking");
		}else throw new IllegalArgumentException("Se ha alcanzado la capacidad "
				+ "máxima del ránking");
	}
	
	/**
	 * @return el producto que se encuentra en la posición consultada a través
	 * del argumento, si esta posición aún no ha sido inicializada, devuelve el
	 * valor null.
	 */
	@Override
	public P getProductByPosition(int posicion) {
		if(posicion >= 0 && posicion < maxSize)
			return productos.get(posicion);
		else throw new IllegalArgumentException("La posición especificada se "
				+ "encuentra fuera del rango de posiciones del ránking");
	}
	
	/**
	 * @return el producto asociado a la referencia pasada como argumento, si 
	 * este se encuentra dentro del ránking, el valor null en caso contrario
	 */
	@Override
	public P getProduct(String prodReference) {
		for(P prod : productos){
			if (prod != null && prod.toString().equals(prodReference))
				return prod;
		}
		return null;
	}
	
	/**
	 * @return la posición que ocupa el producto dentro del ranking por valor,
	 * y en caso de no estar presente, el valor entero -1
	 */
	@Override
	public int getProductPosition(P product){
		return productos.indexOf(product);
	}
	
	/**
	 * @return la posición que ocupa el producto dentro del ranking por
	 * referencia, y en caso de no estar presente, el valor entero -1
	 */
	@Override
	public int getProductPosition(String prodReference){
		P producto = getProduct(prodReference);
		return producto != null ? getProductPosition(producto) : -1;
	}
	
	/**
	 * @return la lista de productos que componen el ranking
	 */
	@Override
	public ArrayList<P> getProducts(){
		return productos;
	}
	
	/**
	 * @return el valor true si el producto pasado como argumento se encuentra
	 * en el ranking
	 */
	@Override
	public boolean containsProduct(P producto) {
		return productos.contains(producto);
	}

	/**
	 * @return el valor true si el producto con referencia prodReference se
	 * encuentra en el ranking
	 */
	@Override
	public boolean containsProduct(String prodReference) {
		return getProduct(prodReference) != null;
	}

	/**
	 * Método para la comparación de 2 rankings de productos; estos deben de 
	 * ser del mismo tipo; dicha precondición se cumple al restringir la 
	 * comparación al tipo parametrizado P con el que ha sido creado el ranking.
	 * 
	 * @return un mapa de pares (producto, diferencia de posiciones) segun lo
	 * establecido en las especificaciones para el cálculo de estas diferencias
	 */
	@Override
	public Map<P,Integer> compareToRanking(IRanking<P> r) {
		Map<P,Integer> difTable = new LinkedHashMap<P,Integer>();
		int diferencia, posicionRef;
		for(int indice = 0; indice<r.getSize(); indice++){
			P prod = r.getProductByPosition(indice);
			posicionRef = getProductPosition(prod);
			/* Si el producto no se encuentra en el ranking de referencia, es 
			 * el equivalente a que hubiese subido posiciones hasta la actual 
			 */
			if(posicionRef == -1) 
				diferencia = r.getSize() - indice;
			else diferencia = posicionRef - indice;
			if(diferencia > 0)
				diferencia *= 2;
			difTable.put(prod, diferencia);
		}
		return difTable;
	}

}
