package uva.tds.concursos;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class EjecutaConcurso {
	public static void main(String [] args){
		
		int nominados = 5;
		
		System.out.println("Se realizan 2 concursos paralelos, preguntando"
				+ " sobre el lenguaje de programación de preferencia a 2 "
				+ "públicos difrentes:");
		
		
		Calendar fIni = Calendar.getInstance();
		Calendar fFin = Calendar.getInstance();
		fFin.add(Calendar.SECOND, 3);
		
		Concurso<String> c = new Concurso<>(nominados,fIni,fFin);
		Concurso<String> cParalelo = new Concurso<>(nominados,fIni,fFin);
		
		String [] productosNominados = 
			{"Java", "Python", "C++", "Haskell", "Perl"};
		
		c.nominar(productosNominados);
		cParalelo.nominar(productosNominados);
		
		for(String lenguaje : productosNominados){
			int votos = (int) (Math.random()*10);
			for(int i=0; i<votos ; i++){
				c.votar(lenguaje);
			}
			votos = (int) (Math.random()*10);
			for(int i=0; i<votos ; i++){
				cParalelo.votar(lenguaje);
			}
		}
		
		try{
			TimeUnit.SECONDS.sleep(3);
		}catch (InterruptedException e){
			System.err.println(e);
		}
		
		c.cerrar();
		cParalelo.cerrar();
		
		System.out.println("El top3 de lenguajes elegidos en cada concurso ha resultado:");
		IRanking<String> r1 = c.obtenerRanking(3);
		IRanking<String> r2 = cParalelo.obtenerRanking(3);
		
		System.out.println("Resultados del primer concurso: ");
		for(int i = 0 ; i<r1.getProducts().size() ; i++){
			System.out.println("#" + (i+1) + ": " + r1.getProductByPosition(i));
		}
		
		System.out.println("Resultados del segundo concurso: ");
		for(int i = 0 ; i<r2.getProducts().size() ; i++){
			System.out.println("#" + (i+1) + ": " + r2.getProductByPosition(i));
		}
		
		System.out.println("Comparación del segundo ránking con respecto al primero: ");
		Map<String, Integer> comparativa = r1.compareToRanking(r2);
		for(String lenguaje : comparativa.keySet()){
			char signo = comparativa.get(lenguaje) >= 0 ? '+' : '\0'; 
			System.out.println(lenguaje + ":     \t" + signo + comparativa.get(lenguaje));
		}
		
	}
	
	public static String fechaString(Calendar fecha){
		return fecha.get(5) + "/" + fecha.get(2) + "/" + fecha.get(1);
	}
}
