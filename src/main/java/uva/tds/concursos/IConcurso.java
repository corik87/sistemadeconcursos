package uva.tds.concursos;

public interface IConcurso<E> {

	public boolean isCerrado();

	public void cerrar();

	public void nominar(E p);

	public void nominar(E[] productos);

	public void votar(E p);

	public IRanking<E> obtenerRanking();

	public IRanking<E> obtenerRanking(int top);

}