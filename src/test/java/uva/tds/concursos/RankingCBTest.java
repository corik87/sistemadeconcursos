package uva.tds.concursos;
import static org.junit.Assert.*;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class RankingCBTest {

	IRanking<Integer> rank;
	Integer p;
	
	@Before
	public void setUp() throws Exception {
		rank = new Ranking<Integer>();
		p = new Integer(100);
		rank.addProduct(p);
	}
	
	@After
	public void tearDown() throws Exception {
		rank = null;
		p = null;
	}
	
	// Creación:
	
	@Test
	public void testDefaulSize() {
		IRanking<Integer> r = new Ranking<Integer>();
		assertEquals(r.getMaxSize(),10);
		assertEquals(r.getSize(),0);
	}
	
	@Test
	public void testFixedSize() {
		int maxSize = 15;
		IRanking<Integer> r = new Ranking<Integer>(maxSize);
		assertEquals(r.getMaxSize(),maxSize);
		assertEquals(r.getSize(),0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNegativeFixedSize() {
		int maxSize = -1;
		IRanking<Integer> r = new Ranking<Integer>(maxSize);
	}
	
	// Req.Ranking.1: Consulta de producto por posición
	
	@Test
	public void testGetProductByPosition() {
		int posicion;
		Integer prod = null;
		for(posicion = 1 ; posicion<4 ; posicion++){
			prod = new Integer(100+posicion);
			rank.addProduct(prod);
		}
		assertEquals(rank.getProductByPosition(--posicion),prod);
	}
	
	@Test
	public void testGetProductByPositionNotFilled() {
		assertTrue(rank.getSize()<rank.getMaxSize());
		assertNull(rank.getProductByPosition(rank.getSize()));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetProductByNegativePosition() {
		int posicion = -1;
		Integer notAProduct = rank.getProductByPosition(posicion);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetProductByMaxPosition() {
		int posicion = rank.getMaxSize();
		Integer notAProduct = rank.getProductByPosition(posicion);
	}
	
	@Test
	public void testGetProductByRef() {
		p = new Integer(101);
		rank.addProduct(p);
		assertEquals(rank.getProduct(p.toString()),p);
	}
	
	@Test
	public void testGetProductNotPresentByRef() {
		p = new Integer(101);
		assertNull(rank.getProduct(p.toString()));
	}
	
	// Req.Ranking.2: Consulta de posición por valor y por referencia:
		
		@Test
		public void testPositionByValue() {
			Integer prod = new Integer(101) ;
			int posicion = rank.getSize();
			rank.addProduct(prod);
			assertEquals(rank.getProductPosition(prod),posicion);
		}
		
		@Test
		public void testPositionByRef() {
			Integer prod = new Integer(101) ;
			int posicion = rank.getSize();
			rank.addProduct(prod);
			assertEquals(rank.getProductPosition(prod.toString()),posicion);
		}
		
		@Test
		public void testPositionNotPresentByValue() {
			Integer prod = new Integer(101) ;
			assertFalse(rank.getProducts().contains(prod));
			assertEquals(rank.getProductPosition(prod),-1);
		}
		
		@Test
		public void testPositionNotPresentByRef() {
			Integer prod = new Integer(101) ;
			assertFalse(rank.getProducts().contains(prod));
			assertEquals(rank.getProductPosition(prod.toString()),-1);
		}
	
	// Req.Ranking.3: Producto en el Ranking, por valor y por referencia:
	
		@Test
		public void testContainsProductValue() {
			Integer prod = new Integer(101) ;
			rank.addProduct(prod);
			assertTrue(rank.containsProduct(prod));
		}
		
		@Test
		public void testContainsProductRef() {
			Integer prod = new Integer(101) ;
			rank.addProduct(prod);
			assertTrue(rank.containsProduct(prod.toString()));
		}
		
		@Test
		public void testDoesNotContainProductValue() {
			Integer prod = new Integer(101) ;
			assertFalse(rank.getProducts().contains(prod));
			assertFalse(rank.containsProduct(prod));
		}
		
		@Test
		public void testDoesNotContainProductRef() {
			Integer prod = new Integer(101) ;
			assertFalse(rank.getProducts().contains(prod));
			assertFalse(rank.containsProduct(prod.toString()));
		}
	
	// Req.Ranking.4: Comparación de 2 rankings de productos del mismo tipo
	
	@Test
	public void testCompareRanks() {
		Integer[] productos = {101,102,103,104,105,106};
		IRanking<Integer> r = new Ranking<Integer>();
		r.addProduct(p);
		
		for(int posicion = 0 ; posicion<productos.length; posicion++){
			if(posicion<3){
				rank.addProduct(productos[posicion]);
				r.addProduct(productos[2-posicion]);
			}
			else if(posicion<5)
				r.addProduct(productos[posicion]);
			else
				rank.addProduct(productos[posicion]);
		}		
		Integer[] diferencias = {0,-2,0,+4,+4,+2};
		
		Map<Integer,Integer> tablaDiferencias = rank.compareToRanking(r);
		
		for(int i=0 ; i<r.getSize() ; i++){
			assertEquals(tablaDiferencias.get(100+i),diferencias[i]);
		}
	}
	
	// Tests de los métodos de añadir producto:
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddProductFullRanking() {
		int maxSize = rank.getMaxSize();
		Integer prod = null;
		for(int posicion = 1 ; posicion<maxSize+1; posicion++){
			prod = new Integer(100+posicion);
			rank.addProduct(prod);
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddProductAlreadyInRanking() {
		Integer producto = new Integer(101);
		for(int i=0 ; i<2 ; i++)
			rank.addProduct(producto);
	}
}