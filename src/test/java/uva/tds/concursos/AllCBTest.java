package uva.tds.concursos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
@RunWith(Suite.class)
@SuiteClasses({
	ConcursoObtencionRankingCBTest.class, 
	ConcursoCBTest.class,
	RankingCBTest.class })
public class AllCBTest {

}
