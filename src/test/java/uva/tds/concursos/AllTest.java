package uva.tds.concursos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
@RunWith(Suite.class)
@SuiteClasses({ 
	ConcursoObtencionRankingTest.class, 
	ConcursoTest.class,
	RankingTest.class})
public class AllTest {

}
