package uva.tds.concursos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class ConcursoTest {
	Calendar fInic;
	Calendar fFin;
	
	@Before
	public void setUp(){
		fInic = Calendar.getInstance();
		fInic.add(Calendar.DAY_OF_MONTH, 7);
		fFin = Calendar.getInstance();
		fFin.add(Calendar.DAY_OF_MONTH, 14);
	}
	
	@After
	public void tearDown(){
		fInic = null;
		fFin = null;
	}

	@Test
	public void testCreaConcursoInteger() {
		Concurso<Integer> concurso = new Concurso<Integer>(1, fInic, fFin);
		assertEquals(1, concurso.maximosNominados); //El maximo de nominados igual al valor pasado
		assertFalse(concurso.isCerrado()); //El concurso no se crea cerrado
		assertNotNull(concurso.concurso);
		assertTrue(fInic.before(fFin));
	}
	
	@Test
	public void testCreaConcursoString() {
		Concurso<String> concurso = new Concurso<String>(1, fInic, fFin);
		assertEquals(1, concurso.maximosNominados);
		assertFalse(concurso.isCerrado());
		assertNotNull(concurso.concurso);
		assertTrue(fInic.before(fFin));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoCreaNegativos() {
		new Concurso<Integer>(-1, fInic, fFin);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoCreaVacios() {
		new Concurso<Integer>(0, fInic, fFin);
	}
	
	@Test
	public void testIsCerrado() {
		Concurso<Integer> concurso = new Concurso<Integer>(1, fInic, fFin);
		assertEquals(concurso.cerrado, concurso.isCerrado());
	}
	
	@Test
	public void testCerrar() {
		fInic.add(Calendar.MONTH, -1);
		fFin.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		concurso.cerrar();
		assertTrue(concurso.isCerrado());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoCerrarSiNoFechaFin(){
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		concurso.cerrar();
	}
		
	@Test(expected=IllegalStateException.class)
	public void testNoCerrarYaCerrados() {
		fInic.add(Calendar.MONTH, -1);
		fFin.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		concurso.cerrar();
		concurso.cerrar();
	}
	
	@Test
	public void testNominar() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		assertFalse(concurso.isCerrado());
		assertTrue(concurso.concurso.containsKey(p));
		assertEquals(concurso.concurso.get(p),new Integer(0)); //Votos inicializados a '0'.
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoNominarNulo() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = null;
		concurso.nominar(p);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoNominarRepetidos() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		concurso.nominar(p);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoNominarSiLleno() {
		Concurso<Integer> concurso = new Concurso<Integer>(1, fInic, fFin);
		Integer p1 = new Integer(101);
		Integer p2 = new Integer(102);
		concurso.nominar(p1);
		concurso.nominar(p2);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoNominarSiCerrado() {
		fInic.add(Calendar.MONTH, -1);
		fFin.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		concurso.cerrar();
		Integer p = new Integer(101);
		concurso.nominar(p);
	}
	
	@Test
	public void testNominarVarios() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p1 = new Integer(101);
		Integer p2 = new Integer(102);
		Integer p3 = new Integer(103);
		Integer[] productos = {p1, p2, p3};
		concurso.nominar(productos);
		assertFalse(concurso.isCerrado());
		for (Integer p : productos){
			assertTrue(concurso.concurso.containsKey(p));
			assertEquals(concurso.concurso.get(p), new Integer(0)); //Votos inicializados a '0'.
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoNominarNulos() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer[] productos = null;
		concurso.nominar(productos);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoNominarAlgunNulo() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p1 = null;
		Integer p2 = new Integer(102);
		Integer p3 = new Integer(103);
		Integer[] productos = {p1, p2, p3};
		concurso.nominar(productos);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoNominarVariosAlgunoYaNominado() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		Integer p1 = new Integer(101);
		Integer p2 = new Integer(102);
		Integer p3 = new Integer(103);
		Integer[] productos = {p1, p2, p3};
		concurso.nominar(productos);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoNominarVariosRepetidos() {
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p1 = new Integer(101);
		Integer p2 = new Integer(101);
		Integer p3 = new Integer(103);
		Integer[] productos = {p1, p2, p3};
		concurso.nominar(productos);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoNominarVariosSiLleno() {
		Concurso<Integer> concurso = new Concurso<Integer>(1, fInic, fFin);
		Integer p1 = new Integer(101);
		Integer p2 = new Integer(102);
		Integer p3 = new Integer(103);
		Integer[] productos = {p1, p2, p3};
		concurso.nominar(productos);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoNominarVariosSiCerrado() {
		fInic.add(Calendar.MONTH, -1);
		fFin.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		concurso.cerrar();
		Integer p1 = new Integer(101);
		Integer p2 = new Integer(102);
		Integer p3 = new Integer(103);
		Integer[] productos = {p1, p2, p3};
		concurso.nominar(productos);
	}
	
	@Test
	public void testVotar() {
		fInic.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		Integer votosP = concurso.concurso.get(p);
		concurso.votar(p);
		assertFalse(concurso.isCerrado());
		assertTrue(concurso.concurso.containsKey(p));
		assertEquals(concurso.concurso.get(p), new Integer(votosP + 1));
		assertNotNull(p);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoVotarSiCerrado() {
		fInic.add(Calendar.MONTH, -1);
		fFin.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		concurso.cerrar();
		concurso.votar(p);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoVotarNoNominado() {
		fInic.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.votar(p);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoVotarNulo() {
		fInic.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		concurso.votar(null);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoVotarSiFechaFin(){
		fInic.add(Calendar.MONTH, -1);
		fFin.add(Calendar.MONTH, -1);
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		concurso.votar(p);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoVotarSiNoFechaInicio(){
		Concurso<Integer> concurso = new Concurso<Integer>(5, fInic, fFin);
		Integer p = new Integer(101);
		concurso.nominar(p);
		concurso.votar(p);
	}
}
