package uva.tds.concursos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class ConcursoObtencionRankingTest {
	Concurso<Integer> concurso;
	Integer[] productos;
	ArrayList<Integer> esperados;
	Calendar fInic;
	Calendar fFin;
	
	@Before
	public void setUp(){
		fInic = Calendar.getInstance();
		fInic.add(Calendar.DAY_OF_MONTH, -7);
		fFin = Calendar.getInstance();
		fFin.add(Calendar.DAY_OF_MONTH, 14);
		concurso = new Concurso<Integer>(20, fInic, fFin);
		productos = new Integer[20];
		for (int i=0; i<productos.length; i++){
			productos[i] = new Integer(100+i);
		}
		concurso.nominar(productos);
		for (int i=0; i<productos.length; i++){
			for (int j=0; j<=i; j++){
				concurso.votar(productos[i]);
			}
		}
		//Simular el fin de las votaciones una vez votados los productos para poder cerrar y obtener ranking.
		concurso.fFinVotaciones = Calendar.getInstance();
	}
	
	@After
	public void tearDown(){
		concurso = null;
		productos = null;
		esperados = null;
	}
	
	@Test
	public void testObtenerRankingTop10() {
		concurso.cerrar();
		
		/* Preparacion del arraylist de esperados para top 10 */
		esperados = new ArrayList<Integer>(10);
		for (int i=0, j=19; i<10; i++, j--){
			esperados.add(productos[j]);
		}
		
		IRanking<Integer> ranking = concurso.obtenerRanking();
		assertNotNull(ranking);
		assertEquals(esperados, ranking.getProducts());
	}
	
	@Test
	public void testObtenerRankingTopN() {
		concurso.cerrar();
		
		/* Preparacion del array de esperados para un N valido */
		int n = 5;
		esperados = new ArrayList<Integer>(n);
		for (int i=0, j=19; i<n; i++, j--){
			esperados.add(productos[j]);
		}
		
		IRanking<Integer> ranking = concurso.obtenerRanking(n);
		assertNotNull(ranking);
		assertEquals(esperados, ranking.getProducts());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoObtenerRankingTopNSiNoCerrado() {
		concurso.obtenerRanking(5);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testNoObtenerRankingTopNSiMenosNominados() {
		concurso.cerrar();
		concurso.obtenerRanking(21);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoObtenerRankingTopNNegativo() {
		concurso.cerrar();
		concurso.obtenerRanking(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNoObtenerRankingTop0() {
		concurso.cerrar();
		concurso.obtenerRanking(0);
	}
}
