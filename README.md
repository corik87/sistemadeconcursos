# Sistema de Concursos
Práctica 3: Tecnologías para el Desarrollo del Software

Grado en Ingeniería Informática: mención Ingeniería del Software

Universidad de Valladolid

## Autores:
* Samuel Alfageme Sainz (samalfa)
* Daniel Barba Gutiérrez (danbarb)

## Descripción:

### Obtención del repositorio:

```
git clone https://salfageme@bitbucket.org/Corik87/sistemadeconcursos.git
cd sistemadeconcursos
```

### Ejecución:

Los distintos objetivos de la práctica pueden conseguirse mediante la ejecución en el shell de los objectivos Ant correspondientes que se describen a continuación:

#### Tareas:
- Compilar 

```
ant compilar
```

- Ejecutar

```
ant ejecutar
```

- Ejecutar los **test de caja negra**

```
ant ejecutarTestTDD
```

- Ejecutar los **test en aislamiento**

```
ant ejecutarTestEnAislamiento
```

- Obtener los informes de cobertura de los tests anteriores

```
ant obtenerInformeCobertura
```

Tras la ejecución de esta tarea, los informes de cobertura pueden ser accedidos desde `target/site/jacoco/index.html`

- Limpiar

```
ant limpiar
```

## Dependencias:

* [OpenJDK 7](http://openjdk.java.net/projects/jdk7/)
* [Ant](http://ant.apache.org/)
* [Maven](http://maven.apache.org/)

El resto de dependencias internas del proyecto se gestionan mendiante Maven.